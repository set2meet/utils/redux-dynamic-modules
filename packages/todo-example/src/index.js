import React from "react";
import { render } from "react-dom";
import { createStore } from "redux-dynamic-modules";
import { Provider } from "react-redux";
import App from "./components/App";
import { combineReducers } from "redux";

const store = createStore({
    advancedNestedCombineReducers: {
        extra: (reducers) => {
            return (state, action) => {
                console.log(state, action.type);
                return combineReducers(reducers)(state, action);
            }
        }
    }});

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
