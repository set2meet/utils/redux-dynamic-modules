import { combineReducers, Reducer, ReducersMapObject, AnyAction } from "redux";
import { getStringRefCounter } from "../Utils/RefCounter";

export interface IReducerManager<S> {
    reduce: (state: S, action: AnyAction) => S;
    getReducerMap: () => ReducersMapObject<S>;
    add: <ReducerState>(key: string, reducer: Reducer<ReducerState>) => void;
    addOrMerge: (key: string, nestedReducers: ReducersMapObject) => void;
    remove: (key: string) => void;
    removeOrUnmerge: (key: string, nestedKeys: string[]) => boolean;
}

export interface IKeysWithReducersToMerge {
    [key:string]: ReducersMapObject<any, AnyAction>,
}

/**
 * Adds reference counting to reducer manager and adds/remove keys only when ref count is zero
 */
export function getRefCountedReducerManager<S>(
    manager: IReducerManager<S>
): IReducerManager<S> {
    const reducerKeyRefCounter = getStringRefCounter();
    for (const key in manager.getReducerMap()) {
        reducerKeyRefCounter.add(key);
    }

    return {
        reduce: manager.reduce,
        getReducerMap: manager.getReducerMap,
        add: <ReducerState>(key: string, reducer: Reducer<ReducerState>) => {
            if (reducerKeyRefCounter.getCount(key) === 0) {
                manager.add(key, reducer);
            }

            reducerKeyRefCounter.add(key);
        },
        addOrMerge(key: string, nestedReducers: ReducersMapObject): void {
            manager.addOrMerge(key, nestedReducers);

            if (reducerKeyRefCounter.getCount(key) === 0) {
                reducerKeyRefCounter.add(key);
            }
        },
        remove: (key: string) => {
            reducerKeyRefCounter.remove(key);

            if (reducerKeyRefCounter.getCount(key) === 0) {
                manager.remove(key);
            }
        },
        removeOrUnmerge: (key: string, nestedKeys: string[]) => {
            const removed = manager.removeOrUnmerge(key, nestedKeys);
            if (removed) {
                reducerKeyRefCounter.remove(key);
            }

            return removed;
        },
    };
}

/**
 * Create a combined reducer as in the fashion of Redux's combineReducers() function,
 * but allows for the dynamic registration of additional reducers
 * @param initialReducers The initial set of reducers
 * @returns An object with three functions: the reducer, an addReducer function, and a removeReducer function
 */
export function getReducerManager<S extends {}>(
    initialReducers: ReducersMapObject<S>,
    reducerCombiner: (
        reducers: ReducersMapObject<S, any>
    ) => Reducer<S> = combineReducers,
    initialReducersToMerge?: IKeysWithReducersToMerge,
    nestedCombiners?: {
        [key: string]: (
            reducers: ReducersMapObject<any, any>
        ) => Reducer
    },
): IReducerManager<S> {
    let combinedReducer = reducerCombiner(initialReducers);
    const reducersMap: ReducersMapObject<S> = {
        ...(initialReducers as object),
    } as ReducersMapObject<S>;
    if (initialReducersToMerge) {
        for (const key in initialReducersToMerge) {
            const combiner = nestedCombiners && nestedCombiners[key] || reducerCombiner;
            reducersMap[key] = getCombinedReducer(initialReducersToMerge[key], combiner);
        }
        combinedReducer = getCombinedReducer(reducersMap, reducerCombiner);
    }

    let keysWithNestedReducers: IKeysWithReducersToMerge = {
        ...initialReducersToMerge,
    };
    let keysToRemove = [];
    let nestedKeysToRemove: {[key:string]: string[]} = {};

    const reduce = (state: S, action: AnyAction) => {
        for (const key in nestedKeysToRemove) {
            if (nestedKeysToRemove[key].length > 0) {
                const nestedState = { ...state[key] };
                for (let nestedKey of nestedKeysToRemove[key]) {
                    delete nestedState[nestedKey];
                }
                state = {
                    ...state,
                    [key]: nestedState,
                }
            }
        }
        nestedKeysToRemove = {};
        if (keysToRemove.length > 0) {
            state = { ...(state as any) };
            for (let key of keysToRemove) {
                delete state[key];
            }
            keysToRemove = [];
        }

        if (state === undefined) {
            state = {} as S;
        }

        return combinedReducer(state, action);
    };

    const remove = (key: string) => {
        if (!key || !reducersMap[key]) {
        return;
        }

        delete reducersMap[key];
        delete keysWithNestedReducers[key];
        keysToRemove.push(key);
        delete nestedKeysToRemove[key];
        combinedReducer = getCombinedReducer(reducersMap, reducerCombiner);
    }

    return {
        getReducerMap: () => reducersMap,
        reduce,
        add: <ReducerState>(key: string, reducer: Reducer<ReducerState>) => {
            if (!key || reducersMap[key]) {
                return;
            }

            reducersMap[key] = reducer;
            combinedReducer = getCombinedReducer(reducersMap, reducerCombiner);
        },
        addOrMerge: (key: string, nestedReducers: ReducersMapObject) => {
            if (!key || !nestedReducers) {
                return;
            }

            keysWithNestedReducers[key] = {
                ...keysWithNestedReducers[key],
                ...nestedReducers,
            }
            const combiner = nestedCombiners && nestedCombiners[key] || reducerCombiner;
            reducersMap[key] = getCombinedReducer(keysWithNestedReducers[key], combiner);
            combinedReducer = getCombinedReducer(reducersMap, reducerCombiner);
        },
        remove,
        removeOrUnmerge: (key: string, nestedKeys: string[]): boolean => {
            if (!key || !reducersMap[key]) {
                return true;
            }

            const _currentKeysToRemove = nestedKeys || [];

            nestedKeys.forEach ((nestedKey) => {
                delete keysWithNestedReducers[key][nestedKey]
            });
            if(Object.keys(keysWithNestedReducers[key]).length === 0) {
                remove(key);
                return true;
            } else {
                const combiner = nestedCombiners && nestedCombiners[key] || reducerCombiner;
                reducersMap[key] = getCombinedReducer(keysWithNestedReducers[key], combiner);
                nestedKeysToRemove[key] = [...(nestedKeysToRemove[key] || []), ..._currentKeysToRemove]
            }

            combinedReducer = getCombinedReducer(reducersMap, reducerCombiner);

            return false;
        },
    };
}

function getCombinedReducer<S extends {}>(
    reducerMap: ReducersMapObject<any>,
    reducerCombiner: (
        reducers: ReducersMapObject<S, any>
    ) => Reducer<S> = combineReducers
) {
    if (!reducerMap || Object.keys(reducerMap).length === 0) {
        return (state, action) => state || null;
    }
    return reducerCombiner(reducerMap);
}
