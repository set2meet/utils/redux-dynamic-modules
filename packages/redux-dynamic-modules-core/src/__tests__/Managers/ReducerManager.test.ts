import {
    getReducerManager,
    getRefCountedReducerManager,
} from "../../Managers/ReducerManager";
import { combineReducers } from "redux";

interface ITestState {
    name: string;
    age: number;
}

const reduce = (state: any, action: any) => state;

it("reducer manager tests", () => {
    const reducerManager = getReducerManager<ITestState>({
        name: reduce,
        age: reduce,
    });

    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "age",
    ]);

    reducerManager.remove("age");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.remove("age");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.remove("city");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.add("city", reduce);
    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "city",
    ]);
});

it("ref counting reducers", () => {
    const reducerManager = getRefCountedReducerManager(
        getReducerManager<ITestState>({
            name: reduce,
            age: reduce,
        })
    );

    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "age",
    ]);

    // Increment the ref count
    reducerManager.add("age", reduce);
    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "age",
    ]);

    // Decrement the ref count
    reducerManager.remove("age");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "age",
    ]);

    // This time it should be removed
    reducerManager.remove("age");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.remove("city");
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.add("city", reduce);
    expect(Object.keys(reducerManager.getReducerMap())).toEqual([
        "name",
        "city",
    ]);
});

it("With a custom combiner", () => {
    let combinedKeys = [];
    function customCombined(reducerMap) {
        console.dir(reducerMap);
        combinedKeys = Object.keys(reducerMap);
    }

    const reducerManager = getReducerManager<ITestState>(
        {
            name: reduce,
            age: reduce,
        },
        customCombined as any
    );

    expect(combinedKeys).toEqual(["name", "age"]);

    reducerManager.remove("age");
    expect(combinedKeys).toEqual(["name"]);

    reducerManager.add("city", reduce);
    expect(combinedKeys).toEqual(["name", "city"]);
});

it("With nested reducers", () => {
    const reduce = (state: any, action: any) => state || null;

    const reducerManager = getReducerManager(
        {
            name: reduce,
        },
    );

    reducerManager.add("age", reduce);
    reducerManager.addOrMerge("age", {"key1": reduce});
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name", "age"]);
    expect(reducerManager.reduce({name: 'test'}, {type: 'ANY'})).toEqual({
        name: 'test',
        age: {
            key1: null,
        }
    });

    reducerManager.removeOrUnmerge("age", ["key1"]);
    expect(Object.keys(reducerManager.getReducerMap())).toEqual(["name"]);

    reducerManager.addOrMerge("age", {"key1": reduce});
    reducerManager.addOrMerge("age", {"key2": reduce});
    expect(reducerManager.reduce({name: 'test'}, {type: 'ANY'})).toEqual({
        name: 'test',
        age: {
            key1: null,
            key2: null,
        }
    });
    reducerManager.remove('name');
    // @ts-ignore
    const prevState = reducerManager.reduce({name: 'test', age: {key1: 'key1', key2: 'key2'}},
        {type: 'ANY'});
    expect(prevState).toEqual({
        age: {
            key1: 'key1',
            key2: 'key2',
        }
    });
    reducerManager.removeOrUnmerge("age", ["key1"]);
    const prevState2 = reducerManager.reduce(prevState, {type: 'ANY'});
    expect(prevState2).toEqual({
        age: {
            key2: 'key2',
        }
    });
    reducerManager.removeOrUnmerge("age", ["key2"]);
    expect(reducerManager.reduce(prevState2, {type: 'ANY'})).toEqual({});
});

it("With a custom combiner and nested reducers", () => {
    let combinedKeys = [];
    let ageNestedKeys = [];
    function customCombined(reducerMap) {
        combinedKeys = Object.keys(reducerMap);
        if(!("age" in reducerMap)) {
            ageNestedKeys = [];
        }
        return combineReducers(reducerMap);
    }

    const nestedCombined = {
        'age': (reducerMap) => {
            ageNestedKeys = Object.keys(reducerMap);
            return combineReducers(reducerMap);
        }
    }

    const reducerManager = getReducerManager(
        {
            name: reduce,
        },
        customCombined,
        {
            age: {
                nestedKey1: reduce,
                nestedKey2: reduce,
            }
        },
        nestedCombined
    );

    expect(combinedKeys).toEqual(["name", "age"]);
    expect(ageNestedKeys).toEqual(["nestedKey1", "nestedKey2"]);

    reducerManager.removeOrUnmerge("age", ["nestedKey1"])

    expect(combinedKeys).toEqual(["name", "age"]);
    expect(ageNestedKeys).toEqual(["nestedKey2"]);

    reducerManager.remove("age");
    expect(combinedKeys).toEqual(["name"]);
    expect(ageNestedKeys).toEqual([]);

    reducerManager.addOrMerge("age", { "nestedKey3": reduce });
    expect(combinedKeys).toEqual(["name", "age"]);
    expect(ageNestedKeys).toEqual(["nestedKey3"]);

    reducerManager.removeOrUnmerge("age", ["nestedKey3"])
    expect(combinedKeys).toEqual(["name"]);
    expect(ageNestedKeys).toEqual([]);
});
